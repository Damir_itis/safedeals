package kpfu.itis.safedeals.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class DealDto {

    @NotBlank(message = "Seller phone number cannot be empty")
    @Pattern(regexp = "^8\\d{10}", message = "Invalid phone number")
    private String sellerPhoneNumber;

    @Positive(message = "Amount must be positive")
    private double amount;

    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @NotEmpty(message = "Description cannot be empty")
    private String description;

}
