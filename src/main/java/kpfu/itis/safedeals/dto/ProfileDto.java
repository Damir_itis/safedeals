package kpfu.itis.safedeals.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ProfileDto {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private int successfulDeals;
    private int totalFeedbacks;
    private double averageUserRating;
    private double systemRating;
    private Date registrationDate;
}
