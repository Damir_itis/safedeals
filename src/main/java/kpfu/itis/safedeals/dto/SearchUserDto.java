package kpfu.itis.safedeals.dto;


import kpfu.itis.safedeals.models.Review;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SearchUserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private int successfulDeals;
    private Long totalDeals;
    private int totalFeedbacks;
    private double averageUserRating;
    private double systemRating;
    private Date registrationDate;
    private List<Review> reviews;
}
