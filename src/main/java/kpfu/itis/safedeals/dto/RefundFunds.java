package kpfu.itis.safedeals.dto;

import lombok.Data;


@Data
public class RefundFunds {
    private Long dealId;
    private Double buyerRefund;
    private Double sellerRefund;
}
