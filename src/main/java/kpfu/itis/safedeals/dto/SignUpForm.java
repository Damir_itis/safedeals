package kpfu.itis.safedeals.dto;

import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class SignUpForm {
    @Size(min = 4, max = 20)
    @NotBlank
    private String firstName;
    @Size(min = 4, max = 20)
    @NotBlank
    private String lastName;
    @Email
    @NotBlank
    private String email;

    @NotEmpty(message = "Обязательное поле")
    @Pattern(regexp="^8\\d{10}$", message = "Invalid phone number")
    private String phoneNumber;

    @NotBlank
    private String password;
}
