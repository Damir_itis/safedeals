package kpfu.itis.safedeals.dto;

import lombok.Data;

@Data
public class ReviewDto {
    private Long dealId;
    private int rating;
    private String message;
}
