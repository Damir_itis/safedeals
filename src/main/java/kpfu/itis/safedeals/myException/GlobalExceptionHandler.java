package kpfu.itis.safedeals.myException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserNotConfirmedException.class)
    public ModelAndView handleUserNotConfirmedException(UserNotConfirmedException ex) {
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        modelAndView.setStatus(HttpStatus.FORBIDDEN);
        return modelAndView;
    }
}
