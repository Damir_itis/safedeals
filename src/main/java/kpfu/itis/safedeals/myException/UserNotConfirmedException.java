package kpfu.itis.safedeals.myException;

public class UserNotConfirmedException extends RuntimeException {

    public UserNotConfirmedException() {
        super("User is not confirmed");
    }

    public UserNotConfirmedException(String message) {
        super(message);
    }

    public UserNotConfirmedException(String message, Throwable cause) {
        super(message, cause);
    }
}

