package kpfu.itis.safedeals.mappers;

import kpfu.itis.safedeals.dto.ProfileDto;
import kpfu.itis.safedeals.models.User;
import org.springframework.stereotype.Component;

@Component
public class ProfileMapper {

    public ProfileDto userToProfileDto(User user) {
        ProfileDto profileDto = new ProfileDto();
        if (user != null) {
            profileDto.setFirstName(user.getName());
            profileDto.setLastName(user.getLastName());
            profileDto.setEmail(user.getEmail());
            profileDto.setPhoneNumber(user.getPhoneNumber());
            profileDto.setSuccessfulDeals(user.getUserStatistics().getSuccessfulDeals());
            profileDto.setTotalFeedbacks(user.getUserStatistics().getTotalFeedbacks());
            profileDto.setAverageUserRating(user.getUserStatistics().getAverageUserRating());
            profileDto.setSystemRating(user.getUserStatistics().getSystemRating());
            profileDto.setRegistrationDate(user.getRegistrationDate());
        }
        return profileDto;
    }
}
