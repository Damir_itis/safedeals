package kpfu.itis.safedeals.mappers;

import kpfu.itis.safedeals.dto.ProfileDto;
import kpfu.itis.safedeals.dto.SearchUserDto;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.services.DealService;
import kpfu.itis.safedeals.services.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SearchUserMapper {

    private final ReviewService reviewService;
    private final DealService dealService;


    public SearchUserDto toSearch(User user) {
        SearchUserDto searchUserDto = new SearchUserDto();
        if (user != null) {
            searchUserDto.setFirstName(user.getName());
            searchUserDto.setLastName(user.getLastName());
            searchUserDto.setEmail(user.getEmail());
            searchUserDto.setPhoneNumber(user.getPhoneNumber());
            searchUserDto.setSuccessfulDeals(user.getUserStatistics().getSuccessfulDeals());
            searchUserDto.setTotalFeedbacks(user.getUserStatistics().getTotalFeedbacks());
            searchUserDto.setAverageUserRating(user.getUserStatistics().getAverageUserRating());
            searchUserDto.setSystemRating(user.getUserStatistics().getSystemRating());
            searchUserDto.setRegistrationDate(user.getRegistrationDate());
            searchUserDto.setTotalDeals(dealService.countAllDealsByUser(user));
            searchUserDto.setReviews(reviewService.findAllByToUser(user));
        }
        return searchUserDto;
    }
}
