package kpfu.itis.safedeals.mappers;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.Review;
import kpfu.itis.safedeals.models.User;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ReviewMapper {
    public Review toReview(User fromUser, User toUser, Deal deal, int rating, String message) {
        return Review.builder()
                .fromUser(fromUser)
                .toUser(toUser)
                .deal(deal)
                .rating(rating)
                .message(message)
                .dateCreated(new Date())
                .build();
    }
}
