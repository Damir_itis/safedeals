package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.Review;
import kpfu.itis.safedeals.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    Review findByDealAndFromUser(Deal deal, User fromUser);

    @Query("SELECT COUNT(r) FROM Review r WHERE r.deal = :deal AND r.fromUser = :user")
    long countByDealAndFromUser(@Param("deal") Deal deal, @Param("user") User user);

    boolean existsByDealAndFromUser(Deal deal, User user);

    @Query("SELECT AVG(r.rating) FROM Review r WHERE r.toUser = :user")
    Double findAverageRatingByUser(@Param("user") User user);

    List<Review> findAllByToUser(User user);
}