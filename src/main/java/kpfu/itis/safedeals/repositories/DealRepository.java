package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRepository extends JpaRepository<Deal,Long> {
    List<Deal> findActiveDealsBySellerOrBuyer(User seller, User buyer);

    List<Deal> findBySeller(User seller);

    List<Deal> findByBuyer(User buyer);
    List<Deal> findAllDealsBySellerOrBuyer(User seller, User buyer);

    @Query("SELECT d FROM Deal d WHERE (d.status = 'COMPLETED' OR d.status = 'REFUSAL') and (d.buyer = :user or d.seller = :user )")
    List<Deal> getArchivedDeals(@Param("user") User user);

    @Query("SELECT d FROM Deal d WHERE d.status = 'COMPLAINT'")
    List<Deal> findAllByStatusComplaint();

    @Query("SELECT d FROM Deal d WHERE (d.status = :status) and (d.buyer = :user or d.seller = :user )")
    List<Deal> findAllByStatus(@Param("status") Deal.Status status, @Param("user") User user);

    @Query("SELECT COUNT(d) FROM Deal d WHERE d.buyer = :user OR d.seller = :user")
    Long countAllDealsByUser(@Param("user") User user);
}
