package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.UserStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStatisticsRepository extends JpaRepository<UserStatistics, Long> {
    @Query("SELECT MAX(us.totalFeedbacks) FROM UserStatistics us")
    int findMaxTotalFeedbacks();

    @Query("SELECT MAX(us.weightedSuccessRate) FROM UserStatistics us")
    double findMaxWeightedSuccessRate();
}
