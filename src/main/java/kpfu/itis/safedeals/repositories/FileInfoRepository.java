package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
}
