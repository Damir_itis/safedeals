package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, Long> {
    int countByFromUserAndDeal(User user, Deal deal);

    @Query("SELECT c FROM Complaint c WHERE c.deal.dealId = :dealId")
    List<Complaint> findAllByDeal_DealId(Long dealId);
}
