package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.ServiceWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ServiceWalletRepository extends JpaRepository<ServiceWallet, Long> {
}
