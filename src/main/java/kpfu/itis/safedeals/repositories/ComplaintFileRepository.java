package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.ComplaintFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintFileRepository extends JpaRepository<ComplaintFile, Long> {
}
