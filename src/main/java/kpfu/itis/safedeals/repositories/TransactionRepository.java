package kpfu.itis.safedeals.repositories;

import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findByUser(User user);
}
