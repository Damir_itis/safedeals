package kpfu.itis.safedeals.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"user"})

public class UserStatistics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int totalDeals;
    private int successfulDeals;
    private int totalFeedbacks;
    private double averageUserRating;
    private double weightedSuccessRate;
    private int systemRating;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
