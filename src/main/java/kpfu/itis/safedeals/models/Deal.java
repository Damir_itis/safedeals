package kpfu.itis.safedeals.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(exclude = {"seller", "buyer"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "deals")
public class Deal {

    public enum Status {
        NEW, IN_PROGRESS, COMPLAINT, COMPLETED, REFUSAL, AWAITING_REVIEW
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deal_id")
    private Long dealId;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private User seller;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    private User buyer;

    private String name;

    private double amount;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    private String description;


}

