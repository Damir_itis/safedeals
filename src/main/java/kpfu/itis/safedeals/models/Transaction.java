package kpfu.itis.safedeals.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "transaction")
public class Transaction {

    public enum Type {
        PURCHASE, SALE, WITHDRAWAL, DEPOSIT
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long transactionId;


    private Type type;
    private double amount; // Сумма транзакции
    private Date date; // Дата транзакции
    private boolean successful;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
