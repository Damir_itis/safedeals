package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.mappers.ReviewMapper;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.Review;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.ReviewRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;

    private final ReviewMapper reviewMapper;

    public ReviewServiceImpl(ReviewRepository reviewRepository, ReviewMapper reviewMapper) {
        this.reviewRepository = reviewRepository;
        this.reviewMapper = reviewMapper;
    }

    public Review createReview(User fromUser, User toUser, Deal deal, int rating, String message) {
        return reviewMapper.toReview(fromUser, toUser, deal, rating, message);
    }

    public void saveReview(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public Review findReviewByDealAndFromUser(Deal deal, User fromUser) {
        return reviewRepository.findByDealAndFromUser(deal, fromUser);
    }

    public boolean hasUserLeftReview(Deal deal, User user) {
        return reviewRepository.countByDealAndFromUser(deal, user) > 0;
    }

    @Override
    public Double findAverageRatingByUser(User user) {
        return reviewRepository.findAverageRatingByUser(user);
    }

    @Override
    public List<Review> findAllByToUser(User user) {
        return reviewRepository.findAllByToUser(user);
    }
}
