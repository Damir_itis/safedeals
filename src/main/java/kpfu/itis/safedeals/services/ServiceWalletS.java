package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.ServiceWallet;

public interface ServiceWalletS {
    ServiceWallet getServiceWallet();
    void setServiceWalletBalance(double amount);

    void save(ServiceWallet serviceWallet);
}
