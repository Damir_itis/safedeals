package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.Review;
import kpfu.itis.safedeals.models.User;

import java.util.List;

public interface ReviewService {
    Review createReview(User fromUser, User toUser, Deal deal, int rating, String message);
    void saveReview(Review review);

    Review findReviewByDealAndFromUser(Deal deal, User fromUser);
    boolean hasUserLeftReview(Deal deal, User user);
    Double findAverageRatingByUser(User user);
    List<Review> findAllByToUser(User user);

}
