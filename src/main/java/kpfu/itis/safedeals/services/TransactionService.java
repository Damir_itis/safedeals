package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;

import java.util.List;

public interface TransactionService {
    void createTransaction(User user, double amount, boolean successful, Transaction.Type type);
    List<Transaction> getTransactionsByUser(User user);
}
