package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.models.UserStatistics;
import kpfu.itis.safedeals.repositories.TransactionRepository;
import kpfu.itis.safedeals.repositories.UserRepository;
import kpfu.itis.safedeals.repositories.UserStatisticsRepository;
import kpfu.itis.safedeals.util.RatingCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserStatisticsRepository userStatisticsRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionService transactionService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserStatisticsRepository userStatisticsRepository,
                           TransactionRepository transactionRepository, TransactionService transactionService) {
        this.userRepository = userRepository;
        this.userStatisticsRepository = userStatisticsRepository;
        this.transactionRepository = transactionRepository;
        this.transactionService = transactionService;
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    @Override
    @Transactional
    public void topUpBalance(User user, double amount) {
        boolean success = false;
        if (amount > 0) {
            try {
                user.setBalance(user.getBalance() + amount);
                userRepository.save(user);
                success = true;
            } finally {
                transactionService.createTransaction(user, amount, success, Transaction.Type.DEPOSIT);
            }
        }
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElse(null);
    }

    @Override
    public User findByPhoneNumber(String phone) {
        return userRepository.findByPhoneNumber(phone).orElse(null);
    }

    @Override
    public User findByEmailOrPhoneNumber(String currentPrincipalName) {
        User buyerByEmail = findByEmail(currentPrincipalName);
        User buyerByPhoneNumber = findByPhoneNumber(currentPrincipalName);
        if (buyerByEmail != null) {
            return buyerByEmail;
        } else {
            return buyerByPhoneNumber;
        }
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void updateSystemRating(User user) {
        UserStatistics statistics = user.getUserStatistics();

        List<Transaction> transactions = transactionRepository.findByUser(user);
        double weightedSuccessRate = transactions.stream()
                .filter(t -> t.getType() == Transaction.Type.PURCHASE || t.getType() == Transaction.Type.SALE)
                .filter(Transaction::isSuccessful) // метод, который возвращает true, если сделка успешная
                .mapToDouble(Transaction::getAmount)
                .sum();
        statistics.setWeightedSuccessRate(weightedSuccessRate);

        int systemRating = RatingCalculator.calculateSystemRating(statistics);
        statistics.setSystemRating(systemRating);
        userStatisticsRepository.save(statistics);
    }

    @Override
    public List<User> findAllByIsSuspicion(boolean s) {
        return userRepository.findAllByIsSuspicion(s);
    }

    private double getMaxWeightedSuccessRate() {

        return userStatisticsRepository.findMaxWeightedSuccessRate();
    }

    private int getMaxFeedbacks() {

        return userStatisticsRepository.findMaxTotalFeedbacks();
    }
}
