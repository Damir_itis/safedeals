package kpfu.itis.safedeals.services;


import kpfu.itis.safedeals.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm signUpForm);
}
