package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.myException.UserNotConfirmedException;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class UserStatusAspect {

    @Autowired
    private UserService userService;

    @Before("@annotation(org.springframework.web.bind.annotation.RequestMapping) && execution(* *(..))")
    public void checkUserStatus() throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails) auth.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userService.getUserById(userId);
        if (user.getState() != User.State.CONFIRMED) {
            throw new UserNotConfirmedException(); // Определяйте свое исключение, например, UserNotConfirmedException
        }
    }
}

