package kpfu.itis.safedeals.services;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import kpfu.itis.safedeals.config.S3Properties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3UploadService {

    private final AmazonS3 amazonS3;
    private final TransferManager transferManager;
    private final S3Properties s3Properties;

    public void uploadFile(MultipartFile file, String key) {
        try {
            Upload upload = transferManager.upload(s3Properties.getBucket(), key, file.getInputStream(), null);
            upload.waitForCompletion();
            log.info("File uploaded successfully to S3 with key: {}", key);
        } catch (IOException | InterruptedException e) {
            log.error("Error uploading file to S3", e);
            Thread.currentThread().interrupt();
        }
    }
}
