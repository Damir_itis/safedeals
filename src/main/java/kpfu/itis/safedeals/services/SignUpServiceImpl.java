package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.dto.SignUpForm;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.models.UserStatistics;
import kpfu.itis.safedeals.repositories.UserRepository;
import kpfu.itis.safedeals.repositories.UserStatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final UserStatisticsRepository userStatisticsRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public SignUpServiceImpl(UserRepository userRepository, UserStatisticsRepository userStatisticsRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userStatisticsRepository = userStatisticsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void signUp(SignUpForm signUpForm) {
        if (!isEmailUnique(signUpForm.getEmail()) || !isPhoneNumberUnique(signUpForm.getPhoneNumber())) {
            throw new IllegalArgumentException("Email or phone number already exists");
        }
        
        User user = User.builder()
                .role(User.Role.USER)
                .state(User.State.CONFIRMED)
                .name(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .phoneNumber(signUpForm.getPhoneNumber())
                .password(passwordEncoder.encode(signUpForm.getPassword()))
                .balance(0)
                .isSuspicion(false)
                .registrationDate(new Date())
                .build();
        userRepository.save(user);

        
        UserStatistics userStatistics = UserStatistics.builder()
                .totalDeals(0)
                .successfulDeals(0)
                .totalFeedbacks(0)
                .averageUserRating(0.0)
                .systemRating(0)
                .user(user)
                .build();
        
        userStatisticsRepository.save(userStatistics);
    }

    private boolean isEmailUnique(String email) {
        return StringUtils.isEmpty(email) || userRepository.findByEmail(email).isEmpty();
    }

    private boolean isPhoneNumberUnique(String phoneNumber) {
        return StringUtils.isEmpty(phoneNumber) || userRepository.findByPhoneNumber(phoneNumber).isEmpty();
    }
}
