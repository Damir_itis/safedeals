package kpfu.itis.safedeals.services;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import jakarta.transaction.Transactional;
import kpfu.itis.safedeals.config.S3Properties;
import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.ComplaintFile;
import kpfu.itis.safedeals.models.FileInfo;
import kpfu.itis.safedeals.repositories.ComplaintFileRepository;
import kpfu.itis.safedeals.repositories.ComplaintRepository;
import kpfu.itis.safedeals.repositories.FileInfoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class FileService {

    private final FileInfoRepository fileRepository;
    private final ComplaintRepository complaintRepository;
    private final AmazonS3 s3;
    private final S3Properties s3Properties;
    private final ComplaintFileRepository complaintFileRepository;
    private final AccessControlList accessControlList;

    @Transactional
    public void upload(MultipartFile file, Complaint complaint) {
        if (file == null) {
            throw new IllegalStateException("File is null!");
        }
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(file.getContentType());
        objectMetadata.setContentLength(file.getSize());
        String fileName = file.getOriginalFilename();
        if (fileName == null) {
            throw new IllegalStateException("File name is null");
        }
        String objectId = "safe/" + UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf("."));
        try (InputStream inputStream = file.getInputStream()) {
            PutObjectRequest putObjectRequest = new PutObjectRequest(s3Properties.getBucket(), objectId, inputStream, objectMetadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            PutObjectResult result = s3.putObject(putObjectRequest);
            log.debug("Uploaded file {}. Etag {}", fileName, result.getETag());
        } catch (IOException e) {
            log.error("Error uploading file {}", fileName);
            throw new IllegalStateException("Error uploading file %s".formatted(fileName), e);
        } catch (SdkClientException e) {
            log.error("Failed to put file {} to s3", fileName);
            throw new IllegalStateException("Failed to put file %s to s3".formatted(fileName), e);
        }

        FileInfo fileInfo = new FileInfo();
        fileInfo.setOriginalFileName(fileName);
        fileInfo.setSize(file.getSize());
        fileInfo.setStorageFileName(objectId);
        fileInfo.setType(file.getContentType());
        fileRepository.save(fileInfo);
        ComplaintFile complaintFile = new ComplaintFile();
        complaintFile.setComplaint(complaint);
        complaintFile.setFileInfo(fileInfo);

        complaintFileRepository.save(complaintFile);
    }
}
