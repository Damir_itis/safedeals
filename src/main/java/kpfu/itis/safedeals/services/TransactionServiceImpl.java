package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class TransactionServiceImpl implements TransactionService{

    private final TransactionRepository transactionRepository;

    @Override
    public void createTransaction(User user, double amount, boolean successful, Transaction.Type type) {
        Transaction transaction = Transaction.builder()
                .user(user)
                .type(type)
                .amount(amount)
                .date(new Date())
                .successful(successful)
                .build();

        transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> getTransactionsByUser(User user) {
        return transactionRepository.findByUser(user);
    }
}
