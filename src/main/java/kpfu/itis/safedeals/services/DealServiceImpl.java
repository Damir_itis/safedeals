package kpfu.itis.safedeals.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import kpfu.itis.safedeals.dto.DealDto;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.DealRepository;
import kpfu.itis.safedeals.repositories.ReviewRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DealServiceImpl implements DealService {

    private final DealRepository dealRepository;
    private final UserService userService;
    private final ServiceWalletS serviceWallet;
    private final ReviewRepository reviewRepository;
    private final RestTemplate restTemplate;


    @Override
    public void createDeal(DealDto dealDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User buyer = userService.findByEmailOrPhoneNumber(currentPrincipalName);

        // Находим пользователя-продавца по номеру телефона
        User seller = userService.findByPhoneNumber(dealDto.getSellerPhoneNumber());

        buyer.setBalance(buyer.getBalance() - dealDto.getAmount());

        serviceWallet.setServiceWalletBalance(serviceWallet.getServiceWallet().getBalance() + dealDto.getAmount());

        // Создаем новую сделку
        Deal deal = new Deal();
        deal.setSeller(seller);
        deal.setBuyer(buyer);
        deal.setName(dealDto.getName());
        deal.setAmount(dealDto.getAmount());
        deal.setDescription(dealDto.getDescription());
        deal.setDateCreated(new Date());
        deal.setStatus(Deal.Status.NEW);

        dealRepository.save(deal);
        checkDeal(deal.getDealId());
    }

    private void checkDeal(Long dealId){
        String url = "http://localhost:8000/predictForUser/" + dealId;
        ResponseEntity<Map> response = restTemplate.postForEntity(url, null, Map.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            Map<String, Object> responseBody = response.getBody();
            Integer isAnomalous = (Integer) responseBody.get("anomalous");

            if (isAnomalous != null) {
                Deal deal = dealRepository.findById(dealId).orElse(null);
                if (deal != null) {
                    User seller = deal.getSeller();
                    User buyer = deal.getBuyer();
                    if (isAnomalous == 1) {
                        seller.setSuspicion(true);
                        buyer.setSuspicion(true);
                        userService.save(seller);
                        userService.save(buyer);
                    }
                }
            }
        }
    }

    @Override
    public List<Deal> getActiveAllDealsForUser(User user) {
        return dealRepository.findActiveDealsBySellerOrBuyer(user, user);
    }

    @Override
    public List<Deal> getActiveDealsForSeller(User seller) {
        return dealRepository.findBySeller(seller);
    }

    @Override
    public List<Deal> getActiveDealsForBuyer(User buyer) {
        return dealRepository.findByBuyer(buyer);
    }

    @Override
    public Deal findDealById(Long dealId) {
        return dealRepository.findById(dealId).orElse(null);
    }

    @Override
    public void save(Deal deal) {
        dealRepository.save(deal);
    }

    @Override
    public List<Deal> getArchivedDeals(User user) {
        return dealRepository.getArchivedDeals(user);
    }

    @Override
    public List<Deal> getAllDealsWithComplaint() {
        return dealRepository.findAllByStatusComplaint();
    }

    @Override
    public List<Deal> getDealsAwaitingReview(User user) {
        return dealRepository.findAllByStatus(Deal.Status.AWAITING_REVIEW, user);
    }

    @Override
    public Long countAllDealsByUser(User user) {
        return dealRepository.countAllDealsByUser(user);
    }

    @Override
    public List<Deal> findAllDealsBySellerOrBuyer(User seller, User buyer) {
        return dealRepository.findAllDealsBySellerOrBuyer(seller, buyer);
    }
}
