package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.ServiceWallet;
import kpfu.itis.safedeals.repositories.ServiceWalletRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@RequiredArgsConstructor
@Service
public class ServiceWalletImpl implements ServiceWalletS {

    private final ServiceWalletRepository serviceWalletRepository;


    @Transactional
    @Override
    public void setServiceWalletBalance(double amount) {
        ServiceWallet serviceWallet = getServiceWallet();
        serviceWallet.setBalance(amount);
    }

    @Override
    public void save(ServiceWallet serviceWallet) {
        serviceWalletRepository.save(serviceWallet);
    }

    @Override
    public ServiceWallet getServiceWallet() {
        return serviceWalletRepository.findById(1L)
                .orElseThrow(() -> new RuntimeException("Service wallet not found"));
    }

}
