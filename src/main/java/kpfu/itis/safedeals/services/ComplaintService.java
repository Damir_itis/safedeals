package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;

import java.util.List;


public interface ComplaintService {
    void save(Complaint complaint);
    boolean hasPermissionToCreateComplaint(User user, Deal deal);
    Complaint createComplaint(User fromUser, User toUser, Deal deal, String descriptionComplaint);

    List<Complaint> findAllByDealId(Long dealId);
}
