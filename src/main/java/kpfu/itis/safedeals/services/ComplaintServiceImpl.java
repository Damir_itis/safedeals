package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.ComplaintRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Service
@RequiredArgsConstructor
public class ComplaintServiceImpl implements ComplaintService {

    private final ComplaintRepository complaintRepository;
    private final DealService dealService;
    private final UserService userService;

    @Override
    public void save(Complaint complaint) {

        complaintRepository.save(complaint);
    }

    public int getComplaintCountForUserAndDeal(User user, Deal deal) {
        return complaintRepository.countByFromUserAndDeal(user, deal);
    }

    @Transactional
    public Complaint createComplaint(User fromUser, User toUser, Deal deal, String descriptionComplaint) {
        Complaint complaint = Complaint.builder()
                .fromUser(fromUser)
                .toUser(toUser)
                .deal(deal)
                .description(descriptionComplaint)
                .dateCreated(new Date())
                .status(Complaint.Status.NEW)
                .build();
        deal.setStatus(Deal.Status.COMPLAINT);
        dealService.save(deal);
        save(complaint);
        return complaint;
    }

    public boolean hasPermissionToCreateComplaint(User user, Deal deal) {
        return isSellerOrBuyer(user, deal)
                && (deal.getStatus() == Deal.Status.IN_PROGRESS || deal.getStatus() == Deal.Status.COMPLAINT)
                && (getComplaintCountForUserAndDeal(user, deal) <= 3);
    }

    private boolean isSellerOrBuyer(User user, Deal deal) {
        return Objects.equals(deal.getSeller().getUser_id(), user.getUser_id()) || Objects.equals(deal.getBuyer().getUser_id(), user.getUser_id());
    }

    @Override
    public List<Complaint> findAllByDealId(Long dealId) {
        return complaintRepository.findAllByDeal_DealId(dealId);
    }
}
