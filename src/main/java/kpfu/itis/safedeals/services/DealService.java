package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.dto.DealDto;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import org.hibernate.query.SelectionQuery;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DealService {
    void createDeal(DealDto dealDto);

    List<Deal> getActiveAllDealsForUser(User user);

    List<Deal> getActiveDealsForSeller(User seller);

    List<Deal> getActiveDealsForBuyer(User buyer);

    Deal findDealById(Long dealId);

    void save(Deal deal);

    List<Deal> getArchivedDeals(User user);

    List<Deal> getAllDealsWithComplaint();

    List<Deal> getDealsAwaitingReview(User user);

    Long countAllDealsByUser(User user);
    List<Deal> findAllDealsBySellerOrBuyer(User seller, User buyer);
}
