package kpfu.itis.safedeals.services;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;

import java.util.List;

public interface UserService {
    User getUserById(Long userId);

    void topUpBalance(User currentUser, double amount);

    User findByEmail(String email);

    User findByPhoneNumber(String phone);

    User findByEmailOrPhoneNumber(String currentPrincipalName);

    List<User> findAllByIsSuspicion(boolean s);

    void save(User user);
    void updateSystemRating(User user);
}
