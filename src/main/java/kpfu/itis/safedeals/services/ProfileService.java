package kpfu.itis.safedeals.services;


import kpfu.itis.safedeals.dto.ProfileDto;
import kpfu.itis.safedeals.mappers.ProfileMapper;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.UserRepository;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {


    private final UserRepository userRepository;
    private final ProfileMapper profileMapper;

    public ProfileService(UserRepository userRepository, ProfileMapper profileMapper) {
        this.userRepository = userRepository;
        this.profileMapper = profileMapper;
    }

    public ProfileDto getUserProfile(Authentication authentication) {
        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            return profileMapper.userToProfileDto(user);
        } else {
            return null;
        }
    }
}