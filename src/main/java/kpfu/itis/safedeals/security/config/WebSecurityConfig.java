package kpfu.itis.safedeals.security.config;

import kpfu.itis.safedeals.security.filters.UserStatusFilter;
import org.apache.catalina.filters.FailedRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    private final DataSource dataSource;
    private final UserStatusFilter userStatusFilter;
    private final UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService, UserStatusFilter userStatusFilter, DataSource dataSource) {
        this.userDetailsService = userDetailsService;
        this.userStatusFilter = userStatusFilter;
        this.dataSource = dataSource;
    }


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.addFilterBefore(userStatusFilter, UsernamePasswordAuthenticationFilter.class);

        RequestCache nullRequestCache = new NullRequestCache();

        http.
                csrf(AbstractHttpConfigurer::disable)

                .formLogin(login -> login
                        .loginPage("/signIn")
                        .defaultSuccessUrl("/main")
                        .failureUrl("/signIn?error")
                        .usernameParameter("emailOrPhone")
                        .passwordParameter("password")
                        .permitAll())


                .authorizeHttpRequests(request ->
                        request.requestMatchers("/signUp").permitAll()
                                .requestMatchers("/main").authenticated()
                                .requestMatchers("/signIn").permitAll()
                                .requestMatchers("/css/**").permitAll()
                                .requestMatchers("/images/**").permitAll()
                                .requestMatchers("/").permitAll()
                                .requestMatchers(HttpMethod.POST,"/user/top-up-balance").authenticated()
                                .requestMatchers(HttpMethod.GET,"/user/info-user").authenticated()
                                .requestMatchers(HttpMethod.GET,"/user/search").authenticated()
                                .requestMatchers(HttpMethod.POST, "/create-deal").authenticated()
                                .requestMatchers(HttpMethod.GET, "/create-deal").authenticated()
                                .requestMatchers(HttpMethod.GET, "/notification").authenticated()
                                .requestMatchers(HttpMethod.GET, "/profile").authenticated()
                                .requestMatchers(HttpMethod.POST, "/confirm-deal/{dealId}").authenticated()
                                .requestMatchers(HttpMethod.POST, "/confirm-payment/{dealId}").authenticated()
                                .requestMatchers(HttpMethod.POST, "/leave-review").authenticated()
                                .requestMatchers(HttpMethod.GET, "/leave-review").authenticated()
                                .requestMatchers(HttpMethod.GET, "/view-deal/{dealId}").authenticated()
                                .requestMatchers(HttpMethod.GET, "/create-complaint/{dealId}").authenticated()
                                .requestMatchers(HttpMethod.GET, "/transaction-history").authenticated()
                                .requestMatchers(HttpMethod.POST, "/create-complaint").authenticated()
                                .requestMatchers(HttpMethod.POST, "/send-complaint/{dealId}").authenticated()
                                .requestMatchers(HttpMethod.POST, "/admin/refund-funds").hasAuthority("ADMIN")
                                .requestMatchers(HttpMethod.POST, "/admin/suspicion-user/{userId}/remove-suspicion").hasAuthority("ADMIN")
                                .requestMatchers(HttpMethod.POST, "/admin/suspicion-user/{userId}/ban").hasAuthority("ADMIN")
                                .requestMatchers(HttpMethod.GET, "/archived-deals").authenticated()
                                .requestMatchers(HttpMethod.GET, "/admin/complaint-deals").hasAuthority("ADMIN")
                                .requestMatchers(HttpMethod.GET, "/admin/deal-complaint").permitAll()
                                .requestMatchers(HttpMethod.GET, "/admin/view-deal-for-admin/{dealId}").hasAuthority("ADMIN")
                                .requestMatchers(HttpMethod.GET, "/admin/suspicion-user/{dealId}").hasAuthority("ADMIN")
                )
//                                .requestMatchers("/files/**").permitAll())

                .rememberMe(rememberMeConfigurer ->
                        rememberMeConfigurer.tokenRepository(tokenRepository())
                                .tokenValiditySeconds(60 * 60 * 4)
                                .rememberMeParameter("rememberMe")
                )

                .logout(httpSecurityLogoutConfigurer ->
                        httpSecurityLogoutConfigurer.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                                .logoutSuccessUrl("/signIn?logout")
                                .deleteCookies("JSESSIONID")
                                .invalidateHttpSession(true))
                .requestCache(cache -> cache.requestCache(nullRequestCache));

        return http.build();
    }


    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PersistentTokenRepository tokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }
}
