package kpfu.itis.safedeals.security.filters;

import java.io.IOException;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@RequiredArgsConstructor
@Component
public class UserStatusFilter extends OncePerRequestFilter {

    private final UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String username = authentication.getName();
            User user = userService.findByEmailOrPhoneNumber(username);

            String currentUrl = request.getRequestURI();
            if (user.getState() != User.State.CONFIRMED &&
                    !currentUrl.equals("/") &&
                    !currentUrl.equals("/notification")) {

                response.sendRedirect("/notification");
                return; // Убедитесь, что цепочка фильтров не продолжается
            }
        }
        filterChain.doFilter(request, response);
    }
}
