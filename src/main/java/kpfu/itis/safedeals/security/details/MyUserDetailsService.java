package kpfu.itis.safedeals.security.details;

import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String emailOrPhoneNumber) throws UsernameNotFoundException {
        Optional<User> userByEmail = userRepository.findByEmail(emailOrPhoneNumber);
        Optional<User> userByPhoneNumber = userRepository.findByPhoneNumber(emailOrPhoneNumber);

        // Проверяем, существует ли пользователь с заданным email или номером телефона
        Optional<User> user;
        String identifierType;

        if (userByEmail.isPresent()) {
            user = userByEmail;
            identifierType = "email";
        } else if (userByPhoneNumber.isPresent()) {
            user = userByPhoneNumber;
            identifierType = "phone";
        } else {
            throw new UsernameNotFoundException("User not found with " + emailOrPhoneNumber);
        }

        return new MyUserDetails(user.orElseThrow(() -> new UsernameNotFoundException("User not found")), identifierType);
    }
}
