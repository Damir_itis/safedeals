package kpfu.itis.safedeals.security.details;

import kpfu.itis.safedeals.models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.Collection;
import java.util.Collections;

public class MyUserDetails implements UserDetails {

    private final User user;
    private final String identifierType;


    public MyUserDetails(User user, String identifierType) {
        this.user = user;
        this.identifierType = identifierType;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(user.getRole().name()));
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        if (identifierType.equals("phone")) {
            return user.getPhoneNumber();
        } else {
            return user.getEmail();
        }
    }
    public String getLastName(){
        return user.getLastName();
    }
    public String getName() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !user.getState().equals(User.State.BANNED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getState().equals(User.State.CONFIRMED);
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public Long getUserId() {
        return user.getUser_id();
    }
}
