package kpfu.itis.safedeals.util;

import kpfu.itis.safedeals.models.UserStatistics;

public class RatingCalculator {

    public static int calculateSystemRating(UserStatistics userStatistics) {
        if (userStatistics == null) {
            throw new IllegalArgumentException("UserStatistics не должен быть null");
        }

        // Получаем данные пользователя из объекта userStatistics
        double weightedSuccessRate = userStatistics.getWeightedSuccessRate();
        int totalFeedbacks = userStatistics.getTotalFeedbacks();
        double averageUserRating = userStatistics.getAverageUserRating();

        // Нормализуем значения
        double maxWeightedSuccessRate = 1000000;
        double maxFeedbacks = 100;

        double normalizedWeightedSuccessRate = weightedSuccessRate / maxWeightedSuccessRate;
        double normalizedTotalFeedbacks = (double) totalFeedbacks / maxFeedbacks;

        if (normalizedWeightedSuccessRate > 1) {
            normalizedWeightedSuccessRate = 1;
        }
        if (normalizedTotalFeedbacks > 1) {
            normalizedTotalFeedbacks = 1;
        }


        double successRateWeight = 0.4;  // Вес взвешенной успешности сделок
        double feedbackWeight = 0.3;  // Вес количества отзывов
        double averageRatingWeight = 0.3;  // Вес среднего рейтинга пользователя

        // Расчет системного рейтинга
        double systemRating = 100 * (
                successRateWeight * normalizedWeightedSuccessRate +
                        feedbackWeight * normalizedTotalFeedbacks +
                        averageRatingWeight * (averageUserRating / 5)
        );

        // Округляем результат до ближайшего целого числа и возвращаем
        return (int) Math.round(systemRating);
    }
}
