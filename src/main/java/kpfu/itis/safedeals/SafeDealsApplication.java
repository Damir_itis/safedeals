package kpfu.itis.safedeals;

import com.amazonaws.services.s3.AmazonS3;
import kpfu.itis.safedeals.config.S3Properties;
import kpfu.itis.safedeals.models.Deal;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SafeDealsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SafeDealsApplication.class, args);
    }

//    @Bean
//    CommandLineRunner commandLineRunner(AmazonS3 s3, S3Properties properties) {
//        return args -> {
//            System.out.println(s3.getBucketAcl(properties.getBucket()));
//        };
//    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
