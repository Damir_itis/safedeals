package kpfu.itis.safedeals.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.CanonicalGrantee;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.concurrent.Executors;

@Slf4j
@Configuration
public class S3Config {
    public static final long FIVE_HUNDRED_MB = (500 * 1024 * 1024);
    public static final int MAX_UPLOAD_THREADS = 5;
    public static final int EXPIRATION_IN_DAYS = 30;

    @Bean
    ClientConfiguration clientConfiguration(S3Properties amazonProperties) {
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTPS);
        clientConfig.setConnectionTimeout(amazonProperties.getConnectTimeout());
        clientConfig.setSocketTimeout(amazonProperties.getReadTimeout());
        return clientConfig;
    }

    @Bean
    TransferManager transferManager(AmazonS3 amazonS3) {
        return TransferManagerBuilder.standard()
                .withS3Client(amazonS3)
                .withMultipartUploadThreshold(FIVE_HUNDRED_MB)
                .withExecutorFactory(() -> Executors.newFixedThreadPool(MAX_UPLOAD_THREADS))
                .build();
    }


    @Bean
    AmazonS3 amazonS3(ClientConfiguration clientConfiguration,
                      S3Properties amazonProperties, S3Properties s3Properties) {
        BasicAWSCredentials credentials = new BasicAWSCredentials(amazonProperties.getAccessKey(), amazonProperties.getSecretKey());

        AmazonS3 s3client = AmazonS3Client.builder()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withClientConfiguration(clientConfiguration)
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(amazonProperties.getHost(), null))
                .build();

        String bucketName = amazonProperties.getBucket();
        if (!s3client.doesBucketExistV2(bucketName)) {
            log.warn("S3 bucket [{}] is not available.", bucketName);
        }

        s3client.setBucketLifecycleConfiguration(bucketName, getLifecycleConfig());
        setBucketAcl(s3Properties, s3client);

        log.info("Created Amazon S3 client for bucket {}", amazonProperties.getBucket());

        return s3client;
    }

    private void setBucketAcl(S3Properties s3Properties, AmazonS3 s3){
        AccessControlList bucketAcl = s3.getBucketAcl(s3Properties.getBucket());
        List<String> grantees = s3Properties.getGrantees();
        grantees.stream().map(CanonicalGrantee::new).forEach(grant -> bucketAcl.grantPermission(grant, Permission.FullControl));
    }


    private BucketLifecycleConfiguration getLifecycleConfig() {
        BucketLifecycleConfiguration.Rule rule = new BucketLifecycleConfiguration.Rule()
                .withId("Delete in 30 days rule")
                .withExpirationInDays(EXPIRATION_IN_DAYS)
                .withStatus(BucketLifecycleConfiguration.ENABLED);

        return new BucketLifecycleConfiguration()
                .withRules(rule);
    }

    @Bean
    public AccessControlList accessControlList(AmazonS3 s3, S3Properties s3Properties) {
        return s3.getBucketAcl(s3Properties.getBucket());
    }
}
