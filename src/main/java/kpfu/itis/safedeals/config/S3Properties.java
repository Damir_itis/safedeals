package kpfu.itis.safedeals.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "application.s3")
public class S3Properties {
    private String accessKey;
    private String secretKey;
    private String bucket;
    private String host;
    private int readTimeout;
    private int connectTimeout;
    private List<String> grantees;
}

