package kpfu.itis.safedeals.listener;

import kpfu.itis.safedeals.models.ServiceWallet;
import kpfu.itis.safedeals.repositories.ServiceWalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StartUpListener implements ApplicationListener<ContextRefreshedEvent> {

    private final ServiceWalletRepository serviceWalletRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (serviceWalletRepository.count() == 0) {
            ServiceWallet serviceWallet = new ServiceWallet();
            serviceWallet.setBalance(0.0);
            serviceWalletRepository.save(serviceWallet);
        }
    }
}
