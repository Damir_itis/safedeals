package kpfu.itis.safedeals.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/signIn")
public class SignInController {

    @GetMapping
    public String getSignInPage(Authentication authentication){
        if (authentication != null){
            return "redirect:/main";
        }
        return "auth/signIn";
    }

//    public String signIn(SignInForm form, BindingResult result, Model model){
//        if (result.hasErrors()){
//            return "signIn";
//        }
//
//        return "redirect:/products";
//    }
}
