package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.services.TransactionService;
import kpfu.itis.safedeals.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class TransactionController {

    private final TransactionService transactionService;

    private final UserService userService;

    public TransactionController(TransactionService transactionService, UserService userService) {
        this.transactionService = transactionService;
        this.userService = userService;
    }

    @GetMapping("/transaction-history")
    public String getTransactionHistory(Principal principal, Model model) {
        User user = userService.findByEmailOrPhoneNumber(principal.getName());
        if (user == null) {
            model.addAttribute("message", "User not authenticated.");
            return "redirect:/main?message=User+not+authenticated";
        }

        List<Transaction> transactions = transactionService.getTransactionsByUser(user);
        model.addAttribute("transactions", transactions);
        return "transaction/transaction-history";
    }
}
