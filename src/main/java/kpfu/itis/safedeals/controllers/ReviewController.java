package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.dto.ReviewDto;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.Review;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.services.DealService;
import kpfu.itis.safedeals.services.ReviewService;
import kpfu.itis.safedeals.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class ReviewController {

    private final ReviewService reviewService;

    private final UserService userService;

    private final DealService dealService;

    public ReviewController(ReviewService reviewService, UserService userService, DealService dealService) {
        this.reviewService = reviewService;
        this.userService = userService;
        this.dealService = dealService;
    }

    @GetMapping("/leave-review")
    public String showReviewForm(@RequestParam Long dealId, Model model, Principal principal) {
        Deal deal = dealService.findDealById(dealId);
        if (deal == null) {
            model.addAttribute("message", "Deal not found.");
            return "redirect:/main?message=Deal+not+found";
        }

        User currentUser = userService.findByEmailOrPhoneNumber(principal.getName());
        if (currentUser == null) {
            model.addAttribute("message", "User not authenticated.");
            return "redirect:/main?message=User+not+authenticated";
        }

        Review existingReview = reviewService.findReviewByDealAndFromUser(deal, currentUser);
        if (existingReview != null) {
            // Отзыв уже оставлен, перенаправляем пользователя или показываем сообщение
            model.addAttribute("message", "You have already left a review for this deal.");
            return "redirect:/main?message=You+have+already+left+a+review+for+this+deal";
        }

        User toUser = currentUser.equals(deal.getBuyer()) ? deal.getSeller() : deal.getBuyer();
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setDealId(dealId);
        model.addAttribute("deal", deal);
        model.addAttribute("toUser", toUser);
        model.addAttribute("reviewDto", reviewDto);
        return "review/create-review";
    }


    @PostMapping("/leave-review")
    public String submitReview(@ModelAttribute ReviewDto reviewDTO, Principal principal, Model model) {
        User fromUser = userService.findByEmailOrPhoneNumber(principal.getName());
        if (fromUser == null) {
            model.addAttribute("message", "User not authenticated.");
            return "redirect:/main?message=User+not+authenticated";
        }

        Deal deal = dealService.findDealById(reviewDTO.getDealId());
        if (deal == null) {
            model.addAttribute("message", "Deal not found.");
            return "redirect:/main?message=Deal+not+found";
        }

        User toUser = fromUser.equals(deal.getBuyer()) ? deal.getSeller() : deal.getBuyer();


        Review review = reviewService.createReview(fromUser, toUser, deal, reviewDTO.getRating(), reviewDTO.getMessage());
        reviewService.saveReview(review);

        boolean hasReviewsFromBoth = reviewService.hasUserLeftReview(deal, deal.getBuyer()) &&
                reviewService.hasUserLeftReview(deal, deal.getSeller());

        // Если оба пользователя оставили отзывы, обновить статус сделки
        if (hasReviewsFromBoth) {
            deal.setStatus(Deal.Status.COMPLETED);
            dealService.save(deal);
        }

        toUser.getUserStatistics().setTotalFeedbacks(toUser.getUserStatistics().getTotalFeedbacks() + 1);

        toUser.getUserStatistics().setAverageUserRating((double) Math.round(reviewService.findAverageRatingByUser(toUser) * 100)/100);
        userService.save(toUser);
        userService.updateSystemRating(toUser);
        userService.updateSystemRating(fromUser);


        return "redirect:/main";
    }
}
