package kpfu.itis.safedeals.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import kpfu.itis.safedeals.dto.DealDto;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.ServiceWallet;
import kpfu.itis.safedeals.models.Transaction;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import kpfu.itis.safedeals.services.DealService;
import kpfu.itis.safedeals.services.ServiceWalletS;
import kpfu.itis.safedeals.services.TransactionService;
import kpfu.itis.safedeals.services.UserService;
import okhttp3.*;

import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;


@Controller
public class DealController {

    private final DealService dealService;
    private final UserService userService;
    private final ServiceWalletS serviceWalletS;
    private final TransactionService transactionService;

    @Autowired
    public DealController(DealService dealService, UserService userService, ServiceWalletS serviceWallet, TransactionService transactionService) {
        this.dealService = dealService;
        this.userService = userService;
        this.serviceWalletS = serviceWallet;
        this.transactionService = transactionService;
    }

    @GetMapping("/create-deal")
    public String getCreateDealPage(@ModelAttribute("dealDto") DealDto dealDto) {
        return "deal/create-deal";
    }

    @PostMapping("/create-deal")
    public String createDeal(@ModelAttribute("dealDto") @Valid DealDto dealDto,
                             BindingResult bindingResult,
                             RedirectAttributes redirectAttributes,
                             Authentication authentication) {
        if (bindingResult.hasErrors()) {
            return "deal/create-deal";
        }
        String currentPrincipalName = authentication.getName();
        User buyer = userService.findByEmailOrPhoneNumber(currentPrincipalName);
        User seller = userService.findByPhoneNumber(dealDto.getSellerPhoneNumber());

        if (seller == null) {
            redirectAttributes.addFlashAttribute("errorMessage", "User with this phone number not found");
            return "redirect:/create-deal";
        }

        if (buyer.getBalance() < dealDto.getAmount()){
            redirectAttributes.addFlashAttribute("errorMessage", "Insufficient funds in the account");
            return "redirect:/main?message=Insufficient+funds+in+the+account";
        }

        if (buyer.getPhoneNumber().equals(seller.getPhoneNumber())) {
            redirectAttributes.addFlashAttribute("errorMessage", "You cannot create a deal with yourself");
            return "redirect:/create-deal";
        }

        if (userService.findByPhoneNumber(dealDto.getSellerPhoneNumber()) == null){
            redirectAttributes.addFlashAttribute("errorMessage", "User with this phone number not found");
            return "redirect:/create-deal";
        }



        try {
            dealService.createDeal(dealDto);
            redirectAttributes.addFlashAttribute("message", "Deal successfully created");
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Failed to create deal: " + e.getMessage());
        }


        return "redirect:/main";
    }

    @GetMapping("/view-deal/{dealId}")
    public String viewDeal(@PathVariable Long dealId, Model model) {
        Deal deal = dealService.findDealById(dealId);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails) auth.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userService.getUserById(userId);
        model.addAttribute("deal", deal);
        model.addAttribute("user", user);
        return "deal/view-deal";
    }

    @PostMapping("/confirm-deal/{dealId}")
    public String confirmDeal(@PathVariable Long dealId, @RequestParam("confirmation") boolean confirmation) {
        Deal deal = dealService.findDealById(dealId);
        User buyer = userService.getUserById(deal.getBuyer().getUser_id());
        User seller = userService.getUserById(deal.getSeller().getUser_id());
        ServiceWallet serviceWallet = serviceWalletS.getServiceWallet();
        if (confirmation) {
            deal.setStatus(Deal.Status.IN_PROGRESS);
        } else {
            deal.setStatus(Deal.Status.REFUSAL);
            serviceWallet.setBalance(serviceWallet.getBalance() - deal.getAmount() * 0.97);
            buyer.setBalance(buyer.getBalance() + deal.getAmount() * 0.97);
        }
        seller.getUserStatistics().setTotalDeals(seller.getUserStatistics().getTotalDeals() + 1);
        buyer.getUserStatistics().setTotalDeals(buyer.getUserStatistics().getTotalDeals() + 1);
        dealService.save(deal);
        userService.save(buyer);
        serviceWalletS.save(serviceWallet);
        return "redirect:/main";
    }

    @PostMapping("/confirm-payment/{dealId}")
    public String confirmPayment(@PathVariable Long dealId, @RequestParam("paymentConfirmation") boolean confirmation) {
        Deal deal = dealService.findDealById(dealId);
        ServiceWallet serviceWallet = serviceWalletS.getServiceWallet();
        User seller = userService.getUserById(deal.getSeller().getUser_id());
        User buyer = userService.getUserById(deal.getBuyer().getUser_id());

        if (confirmation) {
            deal.setStatus(Deal.Status.AWAITING_REVIEW);
            serviceWallet.setBalance(serviceWallet.getBalance() - deal.getAmount() * 0.95);
            seller.setBalance(seller.getBalance() + deal.getAmount() * 0.95);
            seller.getUserStatistics().setSuccessfulDeals(seller.getUserStatistics().getSuccessfulDeals() + 1);
            buyer.getUserStatistics().setSuccessfulDeals(buyer.getUserStatistics().getSuccessfulDeals() + 1);
            transactionService.createTransaction(seller, deal.getAmount(), true, Transaction.Type.SALE);
            transactionService.createTransaction(buyer, deal.getAmount(), true, Transaction.Type.PURCHASE);
            userService.save(buyer);
            dealService.save(deal);
            userService.save(seller);
            serviceWalletS.save(serviceWallet);
            return "redirect:/leave-review?dealId=" + dealId;
        } else {
            transactionService.createTransaction(seller, deal.getAmount(), false, Transaction.Type.SALE);
            transactionService.createTransaction(buyer, deal.getAmount(), false, Transaction.Type.PURCHASE);
            userService.updateSystemRating(seller);
            userService.updateSystemRating(buyer);
            deal.setStatus(Deal.Status.COMPLAINT);
            dealService.save(deal);
            return "redirect:/create-complaint/"+deal.getDealId().toString();
        }
    }



    @GetMapping("/archived-deals")
    public String showArchivedDealsPage(Model model,Authentication authentication) {
        String currentPrincipalName = authentication.getName();
        User user = userService.findByEmailOrPhoneNumber(currentPrincipalName);
        List<Deal> archivedDeals = dealService.getArchivedDeals(user);

        model.addAttribute("archivedDeals", archivedDeals);

        return "deal/archived-deals";
    }
}
