package kpfu.itis.safedeals.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class WelcomeController {

    @GetMapping
    public String showWelcomePage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/main";
        }
        return "welcome";
    }
}