package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.dto.SignUpForm;
import kpfu.itis.safedeals.services.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @Autowired
    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @GetMapping
    public String getSignUpPage(Authentication authentication, Model model) {
        if (!model.containsAttribute("signUpForm")) {
            model.addAttribute("signUpForm", new SignUpForm());
        }
        if (authentication != null) {
            return "redirect:/main";
        }
        return "auth/signUp";
    }


    @PostMapping()
    public String signUp(SignUpForm signUpForm) {

        signUpService.signUp(signUpForm);
        return "auth/signIn";

    }
}
