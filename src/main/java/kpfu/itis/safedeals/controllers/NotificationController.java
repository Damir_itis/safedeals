package kpfu.itis.safedeals.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NotificationController {

    @GetMapping("/notification")
    public String showNotificationPage() {
        return "notification";
    }
}
