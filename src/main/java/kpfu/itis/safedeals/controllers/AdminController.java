package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.dto.RefundFunds;
import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final DealService dealService;
    private final ComplaintService complaintService;
    private final ServiceWalletS serviceWallet;
    private final UserService userService;
    private final ReviewService reviewService;


    @GetMapping("/complaint-deals")
    public String getDealsWithComplaint(Model model) {
        model.addAttribute("dealsWithComplaint", dealService.getAllDealsWithComplaint());
        model.addAttribute("suspiciousUsers", userService.findAllByIsSuspicion(true));
        return "admin/deal-complaint";
    }

    @GetMapping("/view-deal-for-admin/{dealId}")
    public String viewDealForAdmin(@PathVariable Long dealId, Model model) {
        Deal deal = dealService.findDealById(dealId);
        List<Complaint> complaints = complaintService.findAllByDealId(dealId);
        model.addAttribute("deal", deal);
        model.addAttribute("complaints", complaints);
        return "admin/view-deal-for-admin";
    }


    @PostMapping("/refund-funds")
    public String refundFunds(@RequestBody RefundFunds refundFunds,
                              Model model) {

        Deal deal = dealService.findDealById(refundFunds.getDealId());


        if (refundFunds.getBuyerRefund() + refundFunds.getSellerRefund() <= deal.getAmount() && refundFunds.getBuyerRefund() + refundFunds.getSellerRefund() >  deal.getAmount() -  1) {
            serviceWallet.setServiceWalletBalance(serviceWallet.getServiceWallet().getBalance() - deal.getAmount() * 0.95);
            userService.topUpBalance(deal.getBuyer(), refundFunds.getBuyerRefund() * 0.95);
            userService.topUpBalance(deal.getSeller(), refundFunds.getSellerRefund() * 0.95);
            deal.setStatus(Deal.Status.AWAITING_REVIEW);
            dealService.save(deal);
            model.addAttribute("message", "Funds refunded successfully");
        } else {
            // Вернуть страницу с сообщением об ошибке
            model.addAttribute("message", "Refund amount exceeds deal amount");
        }
        return "redirect:/admin/complaint-deals";

    }


    @GetMapping("/suspicion-user/{userId}")
    public String getUserInformation(@PathVariable("userId") Long userId, Model model) {
        User user = userService.getUserById(userId);

        if (user == null) {
            return "redirect:/admin/complaint-deals";
        }

        model.addAttribute("user", user);
        model.addAttribute("reviews", reviewService.findAllByToUser(user));
        model.addAttribute("deals", dealService.findAllDealsBySellerOrBuyer(user, user));
        return "admin/suspicion-user";
    }


    @PostMapping("/suspicion-user/{userId}/ban")
    public String banUser(@PathVariable("userId") Long userId) {
        User user = userService.getUserById(userId);
        if (user != null) {
            user.setState(User.State.BANNED);
            user.setSuspicion(false);
            userService.save(user);
        }
        return "redirect:/admin/complaint-deals";
    }

    @PostMapping("/suspicion-user/{userId}/remove-suspicion")
    public String removeSuspicion(@PathVariable("userId") Long userId) {
        User user = userService.getUserById(userId);
        if (user != null) {
            user.setSuspicion(false);
            userService.save(user);
        }
        return "redirect:/admin/complaint-deals";
    }
}
