package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.dto.ProfileDto;
import kpfu.itis.safedeals.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @GetMapping("/profile")
    public String getProfile(Model model, Authentication authentication) {
        ProfileDto profileDto = profileService.getUserProfile(authentication);
        if (profileDto != null) {
            model.addAttribute("profileDto", profileDto);
            return "user/profile";
        } else {
            return "error";
        }
    }
}
