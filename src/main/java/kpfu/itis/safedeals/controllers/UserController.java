package kpfu.itis.safedeals.controllers;

import com.google.gson.Gson;
import kpfu.itis.safedeals.dto.SearchUserDto;
import kpfu.itis.safedeals.mappers.SearchUserMapper;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import kpfu.itis.safedeals.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final SearchUserMapper searchUserMapper;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);


    public UserController(UserService userService, SearchUserMapper searchUserMapper) {
        this.userService = userService;
        this.searchUserMapper = searchUserMapper;
    }

    @PostMapping("/top-up-balance")
    public String topUpBalance(@RequestParam("amount") double amount,
                               RedirectAttributes redirectAttributes,
                               Authentication authentication){

        if (authentication != null && authentication.isAuthenticated()) {
            MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();

            User userByEmail = userService.findByEmail(userDetails.getUsername());
            User userByPhoneNumber = userService.findByPhoneNumber(userDetails.getUsername());


            if ((userByEmail != null || userByPhoneNumber != null) && amount > 0) {
                if (userByEmail != null) {
                    userService.topUpBalance(userByEmail, amount);
                } else {
                    userService.topUpBalance(userByPhoneNumber, amount);
                }

                return "redirect:/main?message=Balance+successfully+topped+up";
            } else {
                String encodedMessage = URLEncoder.encode("Failed to top up balance. Please try again.", StandardCharsets.UTF_8);
                return "redirect:/main?message=" + encodedMessage;
            }
        }
        return "redirect:/main";
    }

    @GetMapping("/search")
    public String searchUser(@RequestParam String phoneNumber, RedirectAttributes redirectAttributes) {
        User user = userService.findByPhoneNumber(phoneNumber);
        if (user != null) {
            SearchUserDto searchUserDto = searchUserMapper.toSearch(user);
            redirectAttributes.addFlashAttribute("user", searchUserDto);
            return "redirect:/user/info-user";
        } else {
            String message = URLEncoder.encode("User not found.", StandardCharsets.UTF_8);
            return "redirect:/main?message=" + message;
        }
    }

    @GetMapping("/info-user")
    public String showUserPage(@ModelAttribute("user") SearchUserDto user, Model model) {
        model.addAttribute("user", user);
        return "user/info-user";
    }
}

