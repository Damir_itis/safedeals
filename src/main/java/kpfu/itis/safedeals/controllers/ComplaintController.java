package kpfu.itis.safedeals.controllers;


import kpfu.itis.safedeals.models.Complaint;
import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import kpfu.itis.safedeals.services.*;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;


@Controller
@AllArgsConstructor
public class ComplaintController {

    private final DealService dealService;
    private final UserService userService;
    private final ComplaintService complaintService;
    private final FileService fileService;

    @GetMapping("/create-complaint/{dealId}")
    public String showCreateComplaintPage(@PathVariable Long dealId, Model model,
                                          Authentication authentication) {

        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userService.getUserById(userId);
        Deal deal = dealService.findDealById(dealId);
        model.addAttribute("deal", deal);
        model.addAttribute("user", user);
        return "complaint/create-complaint";
    }

    @PostMapping("/create-complaint")
    public String createComplaint(@RequestParam("dealId") Long dealId,
                                  @RequestParam("descriptionComplaint") String descriptionComplaint,
                                  @RequestParam("files") MultipartFile[] files,
                                  Authentication authentication,
                                  RedirectAttributes redirectAttributes) {

        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userService.getUserById(userId);
        Deal deal = dealService.findDealById(dealId);
        User user2 = deal.getBuyer() == user ? deal.getSeller() : deal.getBuyer();

        if (!complaintService.hasPermissionToCreateComplaint(user, deal)) {
            redirectAttributes.addFlashAttribute("message", "You cannot write a complaint about this deal");
            return "redirect:/main?message=You+cannot+write+a+complaint+about+this+deal";
        }

        Complaint complaint = complaintService.createComplaint(user, user2, deal, descriptionComplaint);
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                fileService.upload(file, complaint);
            }
        }

        return "redirect:/main?message=Complaint+sent+successfully";
    }




    @PostMapping("/send-complaint/{dealId}")
    public String sendComplaint(@PathVariable Long dealId, @RequestParam("sendComplaint") boolean sendComplaint) {
        Deal deal = dealService.findDealById(dealId);
        if (sendComplaint){
            deal.setStatus(Deal.Status.COMPLAINT);
            return "redirect:/create-complaint/"+deal.getDealId().toString();
        } else {
            return "redirect:/main";
        }
    }
}
