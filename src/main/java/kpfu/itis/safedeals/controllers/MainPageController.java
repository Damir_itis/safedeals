package kpfu.itis.safedeals.controllers;

import kpfu.itis.safedeals.models.Deal;
import kpfu.itis.safedeals.models.User;
import kpfu.itis.safedeals.security.details.MyUserDetails;
import kpfu.itis.safedeals.services.DealService;
import kpfu.itis.safedeals.services.ReviewService;
import kpfu.itis.safedeals.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
@RequestMapping("/main")
public class MainPageController {

    private final UserService userService;
    private final DealService dealService;
    private final ReviewService reviewService;


    @GetMapping
    public String showMainPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails) auth.getPrincipal();
        Long userId = userDetails.getUserId();
        User user = userService.getUserById(userId);
        List<Deal> activeDeals = dealService.getActiveAllDealsForUser(user);
        List<Deal> awaitingReviewDeals = dealService.getDealsAwaitingReview(user).stream()
                .filter(deal -> !reviewService.hasUserLeftReview(deal, user))
                .toList();

        model.addAttribute("awaitingReviewDeals", awaitingReviewDeals);

        model.addAttribute("activeDeals", activeDeals);
        model.addAttribute("user", user);
        return "mainPage"; // Имя шаблона страницы без расширения
    }

}
