import pandas as pd
from sqlalchemy import create_engine
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
import joblib

def fetch_data_from_db():
    DATABASE_URL = "postgresql://postgres:fIZIKA2002@localhost:5432/SafeDeals"
    engine = create_engine(DATABASE_URL)
    query = """
    SELECT
        d.amount,
        d.date_created as date,
        d.seller_id as user_id,
        'seller' as user_role,
        (SELECT COUNT(*) FROM deals WHERE seller_id = d.seller_id) as deal_frequency,
        us.system_rating as user_rating
    FROM deals d
    JOIN my_user u ON d.seller_id = u.user_id
    JOIN user_statistics us ON u.user_id = us.user_id
    UNION ALL
    SELECT
        d.amount,
        d.date_created as date,
        d.buyer_id as user_id,
        'buyer' as user_role,
        (SELECT COUNT(*) FROM deals WHERE buyer_id = d.buyer_id) as deal_frequency,
        us.system_rating as user_rating
    FROM deals d
    JOIN my_user u ON d.buyer_id = u.user_id
    JOIN user_statistics us ON u.user_id = us.user_id;
    """
    data = pd.read_sql(query, engine)
    return data

data = fetch_data_from_db()

# Признаки для модели
features = ['amount', 'date', 'user_id', 'user_role', 'deal_frequency', 'user_rating']

# Конвертация даты в числовой формат
data['date'] = pd.to_datetime(data['date']).astype('int64') // 10**9


# Подготовка данных
X = data[features]

# Кодирование категориальных признаков
X = pd.get_dummies(X, columns=['user_role'])

feature_names = X.columns.tolist()

# Масштабирование данных
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# Обучение модели Isolation Forest
model = IsolationForest(n_estimators=100, contamination=0.01, random_state=42)
model.fit(X_scaled)

# Сохранение модели, масштабатора и имен признаков
joblib.dump(model, 'isolation_forest_model.pkl')
joblib.dump(scaler, 'scaler.pkl')
joblib.dump(feature_names, 'feature_names.pkl')
print("Модель и скейлер успешно сохранены.")
