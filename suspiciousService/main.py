from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
import models, crud
from database import SessionLocal, engine
import joblib
import pandas as pd

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Загрузка модели, масштабатора и имен признаков
model = joblib.load('isolation_forest_model.pkl')
scaler = joblib.load('scaler.pkl')
feature_names = joblib.load('feature_names.pkl')

# Зависимости для получения сессии базы данных
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/predictForUser/{deal_id}")
async def predict(deal_id: int, db: Session = Depends(get_db)):
    deal = crud.get_deal(db, deal_id)
    if deal is None:
        raise HTTPException(status_code=404, detail="Deal not found")

    user_role = 'seller' if deal.seller_id else 'buyer'
    user_id = deal.seller_id if user_role == 'seller' else deal.buyer_id

    user_statistics = crud.get_user_statistics(db, user_id)
    if user_statistics is None:
        raise HTTPException(status_code=404, detail="User statistics not found")

    deal_frequency = crud.get_deal_frequency(db, user_id, user_role)

    # Преобразование данных сделки в формат DataFrame
    current_deal = {
        'amount': deal.amount,
        'date': deal.date_created.timestamp(),
        'user_id': user_id,
        'user_role': user_role,
        'deal_frequency': deal_frequency,
        'user_rating': user_statistics.system_rating
    }

    current_deal_df = pd.DataFrame([current_deal])

    # Кодирование категориальных признаков
    current_deal_df = pd.get_dummies(current_deal_df, columns=['user_role'])

    for col in feature_names:
        if col not in current_deal_df.columns:
            current_deal_df[col] = 0

    current_deal_df = current_deal_df[feature_names]

    # Масштабирование данных
    current_deal_scaled = scaler.transform(current_deal_df)

    # Предсказание
    prediction = model.predict(current_deal_scaled)

    # Определение результата
    is_anomalous = int(prediction[0] == -1)

    return {"anomalous": is_anomalous}

# Для запуска сервера используйте команду:
# uvicorn main:app --reload
