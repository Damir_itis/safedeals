from sqlalchemy.orm import Session
import models


def get_all_deals(db: Session):
    return db.query(models.Deal).all()


def get_user_statistics(db: Session, user_id: int):
    return db.query(models.UserStatistics).filter(models.UserStatistics.user_id == user_id).first()


def get_deal_frequency(db: Session, user_id: int, user_role: str) -> int:
    if user_role == 'seller':
        count = db.query(models.Deal).filter(models.Deal.seller_id == user_id).count()
    elif user_role == 'buyer':
        count = db.query(models.Deal).filter(models.Deal.buyer_id == user_id).count()
    else:
        count = 0
    return count


def get_deal(db: Session, deal_id: int):
    return db.query(models.Deal).filter(models.Deal.deal_id == deal_id).first()


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.user_id == user_id).first()
