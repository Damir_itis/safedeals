from sqlalchemy import Column, Integer, String, Float, DateTime, Enum, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from database import Base
import enum
from datetime import datetime


class Status(enum.Enum):
    NEW = "NEW"
    IN_PROGRESS = "IN_PROGRESS"
    COMPLAINT = "COMPLAINT"
    COMPLETED = "COMPLETED"
    REFUSAL = "REFUSAL"
    AWAITING_REVIEW = "AWAITING_REVIEW"


class Deal(Base):
    __tablename__ = "deals"

    deal_id = Column(Integer, primary_key=True, index=True)
    seller_id = Column(Integer, ForeignKey("my_user.user_id"), index=True)
    buyer_id = Column(Integer, ForeignKey("my_user.user_id"), index=True)
    name = Column(String, index=True)
    amount = Column(Float, index=True)
    status = Column(Enum(Status), default=Status.NEW)
    date_created = Column(DateTime, default=datetime.utcnow)
    description = Column(String, index=True)



class User(Base):
    __tablename__ = "my_user"

    class Role(enum.Enum):
        USER = "USER"
        ADMIN = "ADMIN"

    class State(enum.Enum):
        NOT_CONFIRMED = "NOT_CONFIRMED"
        CONFIRMED = "CONFIRMED"
        DELETED = "DELETED"
        BANNED = "BANNED"

    user_id = Column(Integer, primary_key=True, autoincrement=True)
    role = Column(Enum(Role))
    state = Column(Enum(State))
    name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    email = Column(String, nullable=False)
    phone_number = Column(String, nullable=False)
    password = Column(String, nullable=False)
    is_suspicion = Column(Boolean, nullable=False, default=False)
    registration_date = Column(DateTime, default=datetime.utcnow)
    balance = Column(Float, default=0.0)

    user_statistics = relationship("UserStatistics", uselist=False, back_populates="user", cascade="all, delete-orphan")


class UserStatistics(Base):
    __tablename__ = "user_statistics"

    id = Column(Integer, primary_key=True, index=True)
    total_deals = Column(Integer)
    successful_deals = Column(Integer)
    total_feedbacks = Column(Integer)
    average_user_rating = Column(Float)
    weighted_success_rate = Column(Float)
    system_rating = Column(Float)
    user_id = Column(Integer, ForeignKey("my_user.user_id"))
    user = relationship("User", back_populates="user_statistics")


User.user_statistics = relationship("UserStatistics", back_populates="user")
